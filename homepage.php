<?php

/*

	Template Name: Home

*/

get_header(); ?>


	<section id="about-statement">
		<div class="wrapper">

			<?php the_field('about_statement'); ?>			

		</div>
	</section>


	<section id="fare">

		<section id="american">
			<a href="<?php the_field('american_fare_link'); ?>" rel="external">
				<img src="<?php $image = get_field('american_fare_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</section>

		<section id="mexican">
			<a href="<?php the_field('mexican_fare_link'); ?>" rel="external">
				<img src="<?php $image = get_field('mexican_fare_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</section>

	</section>

	<section id="contact">
		<div class="wrapper">

			<h2><?php the_field('contact_headline'); ?></h2>

			<div class="contact-options">

				<div class="option menu">
					<div class="info">
						<h4>Menu</h4>

						<?php if(have_rows('menus')): while(have_rows('menus')): the_row(); ?> 
						    <p><a href="<?php the_sub_field('pdf'); ?>"><?php the_sub_field('label'); ?></a></p>
						<?php endwhile; endif; ?>
					</div>
				</div>

				<div class="option phone">
					<div class="info">
						<h4>Phone</h4>
						<p><?php the_field('phone'); ?></p>
					</div>
				</div>

				<div class="option email">
					<div class="info">
						<h4>Email</h4>
						<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
					</div>
				</div>

			</div>

			<div class="contact-statement">
				<?php the_field('contact_info'); ?>
			</div>

		</div>
	</section>


<?php get_footer(); ?>