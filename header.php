<!DOCTYPE html>
<html>
<head>
	<?php the_field('head_meta', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<script src="https://use.typekit.net/bsa8bis.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php the_field('body_meta', 'options'); ?>

	<header>
		<div class="wrapper">

			<div class="logo">
				<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</header>